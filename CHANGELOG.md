# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [4.0.0 Unreleased]

### Changed
- [JOHNSON-20] Now depends on Platform 5 being provided
  
[JOHNSON-20]: https://jira.atlassian.com/browse/JOHNSON-20

