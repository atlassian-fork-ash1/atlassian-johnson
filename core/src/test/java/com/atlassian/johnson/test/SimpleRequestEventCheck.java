package com.atlassian.johnson.test;

import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.RequestEventCheck;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

public class SimpleRequestEventCheck extends SimpleEventCheck implements RequestEventCheck {

    public void check(@Nonnull JohnsonEventContainer eventContainer, @Nonnull HttpServletRequest request) {
    }
}
