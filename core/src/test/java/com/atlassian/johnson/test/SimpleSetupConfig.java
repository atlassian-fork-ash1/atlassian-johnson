package com.atlassian.johnson.test;

import com.atlassian.johnson.setup.SetupConfig;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

public class SimpleSetupConfig extends AbstractInitable implements SetupConfig {

    public static boolean IS_SETUP = false;

    public boolean isSetup() {
        return IS_SETUP;
    }

    public boolean isSetupPage(@Nonnull String uri) {
        return checkNotNull(uri, "uri").contains("setup");
    }
}
